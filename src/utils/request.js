import axios from 'axios';
import { END_POINT, NODE_ENV } from '@/config';
import element from '@/utils/element';
import storage from '@/utils/storage';

// 请求拦截
const defaultRequestInterceptors = (config) => config;
// 响应拦截
const defaultResponseInterceptors = (response) => response;
// 请求开始
const defaultStartLoading = () => {
  console.log('请求开始...');
};
/*
const defaultStartLoading = () => element.load({
  lock: false,
  text: 'Loading',
  spinner: 'el-icon-loading',
  background: 'rgba(0, 0, 0, 0.3)',
});
*/
// 请求结束
const defaultStopLoading = () => {
  console.log('请求结束...');
};
// const defaultStopLoading = () => element.load().close();

class Request {
  constructor(options = {}) {
    this.config = {
      baseURL: options.baseURL,
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: null,
      },
      timeout: 10000, // 单位毫秒
    };
    if (options.headers) {
      this.config = {
        ...this.config,
        headers: {
          ...this.config.headers,
          ...options.headers,
        },
      };
    }
    this.client = axios.create(this.config);
    const requestInterceptors = options.requestInterceptors || defaultRequestInterceptors;
    const responseInterceptors = options.responseInterceptors || defaultResponseInterceptors;
    this.startLoading = defaultStartLoading;
    this.stopLoading = defaultStopLoading;
    this.client.interceptors.request.use(requestInterceptors, (error) => error);
    this.client.interceptors.response.use(responseInterceptors, (error) => responseInterceptors(error.response));
  }

  // 核心请求方法
  send(url, options, params) {
    // console.log('核心请求', url, options, params);
    const token = storage.get(`erp_user_token_${NODE_ENV}`);
    let newConfig = { ...this.config };
    newConfig = Object.assign(newConfig, options);
    newConfig.url = url;
    newConfig.headers.Authorization = `bearer ${token}` || '';
    params && params.isLoading && this.startLoading();
    return this.client.request(newConfig)
      .then((res) => {
        params && params.isLoading && this.stopLoading();
        if (res.data && res.status !== 200) {
          element.setMessageInfo('error', res.data.message, false, () => {
            if (res.status === 401) {
              window.location.href = '/login';
              return;
            }
            res.status === 502 && (window.location.href = '/login');
          });
          return null;
        }
        if (res.data && res.data.access_token) {
          return res.data;
        }
        if (res.data && res.data.code !== 200) {
          if (res.data.code === 101) {
            return res.data;
          }
          element.setMessageInfo('error', res.data.message);
          return null;
        }
        return res.data;
      });
  }
}

const responseInterceptors = (response) => {
  // console.log('response', response);
  if (!response) {
    element.setMessageInfo('error', '系统正在更新，请稍后访问');
    return null;
  }
  const { status } = response;
  if (status !== 200) {
    let message = '';
    switch (status) {
      case 0: message = '没有网络'; break;
      case 400: message = '用户名或密码错误'; break;
      case 401: message = '没有访问该页面的权限'; break;
      case 404: message = '接口请求不存在'; break;
      case 502: message = '系统正在更新，请稍后访问'; break;
      default: message = `未知错误 ${status}--请联系上级管理员`;
    }
    return { data: { code: status, message }, status };
  }
  return response;
};

const request = new Request({
  baseURL: END_POINT,
  responseInterceptors,
});

export default request;

import store from '@/store';

// 新弹出层模块
const setMessageInfo = (mtype, minfo, isback = false, func = null, funcCancel = null) => {
  store.commit('SET_MESSAGE_FLAG', true);
  store.commit('SET_MESSAGE_INFO', {
    mtype,
    minfo,
    isback,
    func,
    funcCancel,
  });
};

export default {
  setMessageInfo,
};

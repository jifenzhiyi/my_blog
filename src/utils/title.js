export default (title) => {
  document.title = title;
  const iframe = document.createElement('iframe');
  iframe.style.visibility = 'hidden';
  iframe.style.width = '1px';
  iframe.style.height = '1px';
  iframe.onload = () => {
    setTimeout(() => iframe.remove(), 0);
  };
  document.body.appendChild(iframe);
};

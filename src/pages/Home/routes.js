const Index = () => import(/* webpackChunkName: "pages/homeIndex" */ './Index');

export default [
  {
    path: '/index',
    name: 'index',
    component: Index,
    meta: {
      title: '首页', requiresAuth: false,
    },
  },
];

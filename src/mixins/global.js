import { mapState } from 'vuex';

export default {
  computed: {
    ...mapState({
      messageFlag: (state) => state.messageFlag,
      messageInfo: (state) => state.messageInfo,
    }),
  },
  methods: {
    setHtmlTitle(name) {
      setTimeout(() => this.$setTitle(name || this.htmlConfig.title), 100);
    },
    messageCancel() {
      this.$store.commit('SET_MESSAGE_FLAG', !this.messageFlag);
    },
    messageSubmit(isback, func) {
      this.$store.commit('SET_MESSAGE_FLAG', !this.messageFlag);
      isback && window.history.back(-1);
      func && func();
    },
    messageConfirm(func) {
      this.$store.commit('SET_MESSAGE_FLAG', !this.messageFlag);
      func();
    },
  },
};

import Vue from 'vue';
import '@/components/index';
import '@/styles/app.less';
// import '@/styles/pop.less';
import setTitle from '@/utils/title';
import element from '@/utils/element';

import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;
// vue 全局变量
Vue.prototype.$setTitle = setTitle;
Vue.prototype.$element = element;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

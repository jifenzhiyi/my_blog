import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// TODO ajax操作公共方法
const ajaxStart = ({ state, commit }, type, params, column) => {
  state[type].listLoading = true;
  state[type].pageNum = params.pageNumber;
  commit('SET_TABLE_COLUMN', { type, column });
};

const ajaxEnd = ({ commit, state }, type, data, totalCount, pageCount) => {
  state[type].listLoading = false;
  commit('SET_TABLE_DATA', { type, data }); // data
  commit('SET_PAGE_TOTAL', { type, num: totalCount });
  commit('SET_NUM_TOTAL', { type, num: pageCount });
};

const ajaxResult = (res, commit, state) => {
  if (res) {
    ajaxEnd({ commit, state }, 'primary', res.data.records, res.data.total, res.data.pages);
  } else {
    ajaxEnd({ commit, state }, 'primary', [], 0, 0);
  }
};

export default new Vuex.Store({
  state: {
    token: null, // 用户token
    messageFlag: false, // 消息模块显示
    messageInfo: {}, // 消息信息
    primary: {
      pageSize: 20, // 列表显示条数
      pageTotal: 0, // 列表总条数
      numTotal: 0, // 列表总页数
      pageNum: 1, // 当前页码
      tableData: [], // 列表数据
      tableColumn: [], // 列表列模块
      listLoading: false, // 列表loading
      tableHeight: 0, // 列表高度
    },
  },
  modules: {},
  mutations: {
    // 设置系统版本号
    SET_VERSION(state, version) {
      state.version = version;
    },
    // 是否显示弹出层
    SET_MESSAGE_FLAG(state, flag) {
      state.messageFlag = flag;
    },
    // 弹出层显示信息
    SET_MESSAGE_INFO(state, info) {
      state.messageInfo = info;
    },
    // 设置列表列
    SET_TABLE_COLUMN(state, obj) {
      state[obj.type].tableColumn = obj.column;
    },
    // 获取列表数据
    SET_TABLE_DATA(state, obj) {
      state[obj.type].tableData = obj.data;
    },
    // 修改列表显示条数
    SET_PAGE_SIZE(state, obj) {
      state[obj.type].pageSize = obj.num;
    },
    // 修改页码
    SET_PAGE_NUM(state, obj) {
      state[obj.type].pageNum = obj.num;
    },
    // 修改列表总条数
    SET_PAGE_TOTAL(state, obj) {
      state[obj.type].pageTotal = obj.num;
    },
    // 修改列表总页数
    SET_NUM_TOTAL(state, obj) {
      state[obj.type].numTotal = obj.num;
    },
  },
  actions: {
    // 操作日志
    async GET_LOG({ commit, state }, params) {
      const column = [
        { prop: 'name', label: '姓名' },
        { prop: 'operationAt', label: '操作时间' },
        { prop: 'operationContent', label: '操作内容' },
        { prop: 'ip', label: 'IP' },
      ];
      ajaxStart({ commit, state }, 'primary', params, column);
      // const res = await logApi.getLog(params);
      const res = {
        code: 200,
        data: {
          records: [],
          total: 10,
          pages: 1,
        },
        message: 'success',
      };
      ajaxResult(res, commit, state);
    },
  },
});

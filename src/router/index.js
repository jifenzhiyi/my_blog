import Vue from 'vue';
import Router from 'vue-router';
import appRoutes from '@/router/routes';
import storage from '@/utils/storage';
import store from '@/store';

const NODE_ENV = process.env.NODE_ENV || 'development';

Vue.use(Router);

const routes = [
  { path: '/', redirect: '/index' },
  { path: '/redirect/:path*', component: () => import('@/components/Redirect') },
  ...appRoutes,
];

const rootRouter = new Router({ mode: 'history', routes });
let firstShow = true;
const initStorage = () => {
  const token = storage.get(`erp_user_token_${NODE_ENV}`);
  token && store.commit('SET_TOKEN', token);
};
// 导航守卫
rootRouter.beforeEach((to, from, next) => {
  if (firstShow) {
    firstShow = false;
    initStorage();
  }
  const requiresAuth = to.meta && (to.meta.requiresAuth || false);
  const userInfo = storage.get(`erp_user_info_${NODE_ENV}`);
  userInfo && store.commit('SET_USER_INFO', userInfo);
  // 需要（重新）登陆
  const needLogin = requiresAuth && !userInfo;
  if (needLogin) {
    next({ path: '/login' });
    return;
  }
  next();
});

export default rootRouter;

import NotFound from '@/components/NotFound';

const req = require.context('@/pages', true, /^\.\/((?!\/)[\s\S])+\/routes.js$/);
let Routes = req.keys().map((path) => req(path).default);
Routes = [].concat(...Routes);

export default [
  ...Routes,
  {
    path: '*',
    name: 'not-found',
    component: NotFound,
  },
];

export const NODE_ENV = process.env.NODE_ENV || 'development';
export const DEBUG = NODE_ENV === 'development';

// 后台接口
export const API_LIST = {
  development: 'https://development/',
  staging: 'https://staging/',
  production: 'https://production/',
};
export const END_POINT = API_LIST[NODE_ENV];

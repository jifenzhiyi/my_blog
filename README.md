# 项目初始化搭建

## 项目目录和配置文件简介
```shell
├── Readme.md                   // 帮助文档
├── public                      // 静态文件资源和入口页面
├── src                         // 源代码目录
│   ├── assets                  // 资源目录（图片）
│   ├── components              // 公共模块
│   ├── mixins                  // 公共的方法或者计算属性
│   ├── pages                   // 页面目录(按功能模块建立目录)
│   │     ├── Home              // 首页
|   |     ├── Login             // 登录页
|   |     └── XXX               // 其它
│   ├── router                  // 路由配置
│   ├── styles                  // 公共css样式
│   ├── utils                   // 公共js方法
│   ├── App.vue                 // 项目入口文件
│   ├── config.js               // 配置文件 ajax路径
│   ├── main.js                 // 项目的核心文件
│   └── store.js                // 项目的状态管理
├── .eslintrc                   // eslint规则 
└── vue.config.js               // webpack配置文件
```

## 开发注意事项
```shell
> - 项目打包目录 `dist`
> - 小图片放在 `src/assets` 下，需重新启动生效
> - 大图片上传服务器，用绝对地址访问，使用CDN
> - 不允许为原生标签写样式，一律使用 `class`
> - 热更新不生效，重新启动
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

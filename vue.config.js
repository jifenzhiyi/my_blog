module.exports = {
  lintOnSave: true, // eslint 是否在保存的时候检查
  devServer: {
    port: 8010,
    host: '0.0.0.0',
    open: true,
  },
  configureWebpack: (config) => {
    config.externals = { vue: 'Vue' };
  },
};
